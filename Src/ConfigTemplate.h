#ifndef ConfigTemplate_h__
#define ConfigTemplate_h__

#include <QString>

#ifdef _MAIN_CPP

QString cfg_template = QString::fromLocal8Bit("\
	<root>																											\n\
		<engine_git_url>https://gitee.com/xfcode/SkyGameEngine2d</engine_git_url>									\n\
		<engine_help>http://xfcode.gitee.io/skygameengine2d/annotated.html</engine_help>							\n\
		<engine_guide>https://blog.csdn.net/qq_33775402/column/info/38797</engine_guide>							\n\
		<last_engine_path></last_engine_path>																		\n\
		<game_project_list>																							\n\
		</game_project_list>																						\n\
	</root>																											\n\
");


#endif // _MAIN_CPP



#endif // ConfigTemplate_h__
