#ifndef ProListView_h__
#define ProListView_h__

#include <QListView>
#include <QMenu>
#include <QAction>
#include <QMouseEvent>
#include <QUrl>
#include <QDesktopServices>
#include "GloableData.h"
#include "Tool.h"
#include "Config.h"
#include "ProListModle.h"

class ProListView
	:public QListView
{
	Q_OBJECT


public:
	ProListView(QWidget* w)
		:QListView(w)
	{
		m_proListMenu = new QMenu(this);
		m_openCurrentSelectPro = new QAction(m_proListMenu);
		m_deleteCurrentSelectPro = new QAction(m_proListMenu);

		m_openCurrentSelectPro->setText(_STR("打开所在文件夹"));
		m_deleteCurrentSelectPro->setText(_STR("在列表中移除"));

		m_proListMenu->addAction(m_openCurrentSelectPro);
		m_proListMenu->addAction(m_deleteCurrentSelectPro);

		connect(m_openCurrentSelectPro, &QAction::triggered, this, [this]() {
			auto index = this->currentIndex();
			if (index.isValid())
			{
				auto path = index.data(Qt::UserRole + 2).toString();
				QUrl url(path);
				QDesktopServices::openUrl(url);
			}
			});

		connect(m_deleteCurrentSelectPro, &QAction::triggered, this, [this]() {
			auto index = this->currentIndex();
			if (index.isValid())
			{
				auto name= index.data(Qt::UserRole + 1).toString();
				Config::getInstance()->removeProjectData(GloableData::getInstance()->getConfigPath(),name);
				ProListModle::getInstance()->removeDate(index.row());
			}
			});
	}

private:
	QMenu* m_proListMenu;
	QAction* m_openCurrentSelectPro;
	QAction* m_deleteCurrentSelectPro;
protected:

	virtual void mouseReleaseEvent(QMouseEvent* event) override
	{
		if (event->button() == Qt::RightButton)
		{
			auto index = this->indexAt(event->pos());
			if (index.isValid())
			{
				m_proListMenu->move(event->globalPos());
				m_proListMenu->exec();
			}
		}
	}

public:


};

#endif // ProListView_h__
