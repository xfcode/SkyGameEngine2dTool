#ifndef Tool_h__
#define Tool_h__

#include <QFile>
#include <QDir>

/*!
\brief 单例对象的基类
*/
template <typename T>
class Singleton
{
public:
	//! 获取单例对象的实例
	/*!
	\return T*
	*/
	static T* getInstance();
};

template <typename T>
T* Singleton<T>::getInstance()
{
	static T t;
	return &t;
}

class FileHelper
{
public:
	//拷贝文件：
	static bool copyFileToPath(QString src_path, QString des_path, bool coverFileIfExist)
	{
		des_path.replace("\\", "/");
		src_path.replace("\\", "/");
		if (src_path == des_path)
		{
			return true;
		}
		if (!QFile::exists(src_path))
		{
			return false;
		}

		bool exist = QFile::exists(des_path);
		if (exist)
		{
			if (coverFileIfExist)
			{
				QFile::remove(des_path);
			}
		}

		if (!QFile::copy(src_path, des_path))
		{
			return false;
		}
		return true;
	}

	//拷贝文件夹：
	static bool copyDirectoryFiles(const QString& fromDir, const QString& toDir, bool coverFileIfExist)
	{
		QDir sourceDir(fromDir);
		QDir targetDir(toDir);
		if (!targetDir.exists())
		{   //如果目标目录不存在，则进行创建 
			if (!targetDir.mkdir(targetDir.absolutePath()))
				return false;
		}

		QFileInfoList fileInfoList = sourceDir.entryInfoList();
		foreach(QFileInfo fileInfo, fileInfoList)
		{
			if (fileInfo.fileName() == "." || fileInfo.fileName() == "..")
				continue;

			if (fileInfo.isDir())
			{   //当为目录时，递归的进行copy 
				if (!copyDirectoryFiles(fileInfo.filePath(),
					targetDir.filePath(fileInfo.fileName()),
					coverFileIfExist))
					return false;
			}
			else
			{   //当允许覆盖操作时，将旧文件进行删除操作 
				if (coverFileIfExist && targetDir.exists(fileInfo.fileName()))
				{
					targetDir.remove(fileInfo.fileName());
				}

				// 进行文件copy
				if (!QFile::copy(fileInfo.filePath(),
					targetDir.filePath(fileInfo.fileName())))
				{
					return false;
				}
			}
		}
		return true;
	}


	static bool copyDirectorySpecifiedFiles(const QString& src_dir, const QStringList& files, const QString& des_dir, bool coverFileIfExist)
	{
		QDir sourceDir(src_dir);
		QDir targetDir(des_dir);
		if (!targetDir.exists())
		{   //如果目标目录不存在，则进行创建 
			if (!targetDir.mkdir(targetDir.absolutePath()))
				return false;
		}

		foreach(auto filename, files)
		{
			//当允许覆盖操作时，将旧文件进行删除操作 
			if (coverFileIfExist && targetDir.exists(filename))
			{
				targetDir.remove(filename);
			}

			// 进行文件copy
			if (!QFile::copy(sourceDir.filePath(filename),
				targetDir.filePath(filename)))
			{
				return false;
			}
		}

		return true;
	}


};


#define SINGLETON_CLASS(T) friend class Singleton<T>;


#define _STR(str) QString::fromLocal8Bit(str)

#endif // Tool_h__
