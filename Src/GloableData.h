#ifndef GloableData_h__
#define GloableData_h__

#include <QString>
#include <QObject>
#include <QCoreApplication>
#include "Tool.h"


class GloableData
	:public QObject, public Singleton<GloableData>
{
	SINGLETON_CLASS(GloableData)

		Q_OBJECT

protected:
	GloableData()
		:m_engineInfoFileName(_STR("engine_info.xml"))
		, m_configPath(QCoreApplication::applicationDirPath() + "/conf/" + "conf.xml")
	{

	}

signals:
	void changeEnginePath();
	void changeEngineVersion();

public:

	const QString& getEnginePath()
	{
		return m_enginePath;
	}

	void setEnginePath(const QString& path)
	{
		m_enginePath = path;
		emit this->changeEnginePath();
	}

	const QString& getEngineVersion()
	{
		return m_currentVersions;
	}

	void setEngineVersion(const QString& v)
	{
		m_currentVersions = v;
		emit this->changeEngineVersion();
	}

	const QString& getEngineInfoFileName()const
	{
		return m_engineInfoFileName;
	}

	const QString& getConfigPath()const
	{
		return m_configPath;
	}

	const QString& getEngineHelp()const
	{
		return m_engineHelp;
	}

	const QString& getEngineGuide()const
	{
		return m_engineGuide;
	}

	const QString& getEngineGitCode()const
	{
		return m_engineGitCode;
	}

	void setEngineHelp(const QString& help_url)
	{
		m_engineHelp = help_url;
	}

	void setEngineGuide(const QString& guide_url)
	{
		m_engineGuide = guide_url;
	}

	void setEngineGitCode(const QString& git_code_url)
	{
		m_engineGitCode = git_code_url;
	}

	

private:
	QString m_enginePath;
	QString m_currentVersions;
	QString m_engineInfoFileName;
	QString m_configPath;
	QString m_engineHelp;
	QString m_engineGitCode;
	QString m_engineGuide;
};


#endif // GloableData_h__
