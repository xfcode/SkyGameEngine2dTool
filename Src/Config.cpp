#include "Config.h"
#include <QString>
#include <string>
#include "ProListModle.h"

bool Config::loadConfig(const QString& path)
{
	using namespace tinyxml2;
	tinyxml2::XMLDocument doc;
	auto err = doc.LoadFile(path.toStdString().c_str());
	if (err != tinyxml2::XML_SUCCESS)
	{
		return false;
	}

	auto root = doc.RootElement();
	if (!root)
	{
		return false;
	}

	auto engine_path = root->FirstChildElement("last_engine_path");
	if (engine_path)
	{
		QString last_engine_path = _STR(engine_path->GetText());
		if (!last_engine_path.isEmpty())
		{
			GloableData::getInstance()->setEnginePath(last_engine_path);
		}
	}

	auto engine_help = root->FirstChildElement("engine_help");
	if (engine_help)
	{
		QString engine_help_url = _STR(engine_help->GetText());
		if (!engine_help_url.isEmpty())
		{
			GloableData::getInstance()->setEngineHelp(engine_help_url);
		}
	}

	auto engine_guide = root->FirstChildElement("engine_guide");
	if (engine_guide)
	{
		QString engine_guide_url = _STR(engine_guide->GetText());
		if (!engine_guide_url.isEmpty())
		{
			GloableData::getInstance()->setEngineGuide(engine_guide_url);
		}
	}

	auto engine_gitcode = root->FirstChildElement("engine_git_url");
	if (engine_gitcode)
	{
		QString engine_gitcode_url = _STR(engine_gitcode->GetText());
		if (!engine_gitcode_url.isEmpty())
		{
			GloableData::getInstance()->setEngineGitCode(engine_gitcode_url);
		}
	}

	auto game_project_list_e = root->FirstChildElement("game_project_list");
	if (!game_project_list_e)
	{
		return false;
	}

	auto project_e = game_project_list_e->FirstChildElement("game_project");
	while (project_e)
	{
		QString name;
		QString path;
		QString time;

		auto name_e = project_e->FirstChildElement("name");
		if (name_e)
		{
			name = _STR(name_e->GetText());
		}

		auto path_e = project_e->FirstChildElement("path");
		if (path_e)
		{
			path = _STR(path_e->GetText());
		}

		auto time_e = project_e->FirstChildElement("time");
		if (time_e)
		{
			time = _STR(time_e->GetText());
		}

		ProListModle::getInstance()->appendProData(ProjectData{
			name,path,time
			}
		);

		project_e = project_e->NextSiblingElement("game_project");
	}

	return true;
}

bool Config::wirteConfig(const QString& path, const QString& key, const QString& value)
{
	using namespace tinyxml2;
	tinyxml2::XMLDocument doc;
	auto err = doc.LoadFile(path.toStdString().c_str());
	if (err != tinyxml2::XML_SUCCESS)
	{
		return false;
	}

	auto root = doc.RootElement();
	if (!root)
	{
		return false;
	}

	auto value_e = root->FirstChildElement(key.toStdString().c_str());
	if (!value_e)
	{
		return false;
	}

	value_e->SetText(value.toStdString().c_str());

	doc.SaveFile(path.toStdString().c_str());

	return true;
}

bool Config::appendProjectData(const QString& path, const ProjectData& data)
{
	using namespace tinyxml2;
	tinyxml2::XMLDocument doc;
	auto err = doc.LoadFile(path.toStdString().c_str());
	if (err != tinyxml2::XML_SUCCESS)
	{
		return false;
	}

	auto root = doc.RootElement();
	if (!root)
	{
		return false;
	}

	auto game_project_list_e = root->FirstChildElement("game_project_list");
	if (!game_project_list_e)
	{
		return false;
	}

	XMLElement* game_project = doc.NewElement("game_project");
	XMLElement* name_e = doc.NewElement("name");
	name_e->SetText(data.name.toStdString().c_str());
	XMLElement* path_e = doc.NewElement("path");
	path_e->SetText(data.path.toStdString().c_str());
	XMLElement* time_e = doc.NewElement("time");
	time_e->SetText(data.time.toStdString().c_str());

	game_project->InsertFirstChild(name_e);
	game_project->InsertEndChild(path_e);
	game_project->InsertEndChild(time_e);
	game_project_list_e->InsertFirstChild(game_project);

	doc.SaveFile(path.toStdString().c_str());

	return true;
}

bool Config::removeProjectData(const QString& path,const QString& name)
{
	using namespace tinyxml2;
	tinyxml2::XMLDocument doc;
	auto err = doc.LoadFile(path.toStdString().c_str());
	if (err != tinyxml2::XML_SUCCESS)
	{
		return false;
	}

	auto root = doc.RootElement();
	if (!root)
	{
		return false;
	}

	auto game_project_list_e = root->FirstChildElement("game_project_list");
	if (!game_project_list_e)
	{
		return false;
	}

	auto project_e = game_project_list_e->FirstChildElement("game_project");
	while (project_e)
	{
		auto name_e = project_e->FirstChildElement("name");
		if (name_e)
		{
			if (_STR(name_e->GetText()) == name)
			{
				game_project_list_e->DeleteChild(project_e);
				break;
			}
		}
		project_e = project_e->NextSiblingElement("game_project");
	}

	doc.SaveFile(path.toStdString().c_str());

	return true;
}

