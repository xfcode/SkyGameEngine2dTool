﻿#pragma once

#include <QtWidgets/QMainWindow>
#include <QFileDialog>
#include <QGraphicsDropShadowEffect>

#include "ui_MainWidget.h"

class TaskerNewProject;

class MainWidget : public QMainWindow
{
	Q_OBJECT

public:
	MainWidget(QWidget* parent = Q_NULLPTR);

private slots:

	void onBntAddEnginePath();
	void onBntAddProjectDir();
	void onBntClose();

	void addLog(const QString& log);
	void addLog(const char* log);
	void addErr(const QString& err);
	void addErr(const char* err);

	void onChangeEnginePath();
	void onChangeEngineVersion();
	
	//创建一个新游戏
	void onBntCreateNewGame();

	void onCreateNewGameComplate(int code);
private:
	void createNewProject(const QString& dir, const QString& name);

private:
	Ui::MainWidgetClass ui;

	TaskerNewProject* m_taskerNewPro;

	bool m_isEnginePathOk;

	bool m_isCreateProOpenProDir;

	QString m_currnetCreateProDir;
	QString m_createProName;

	bool m_isDrag;
	QPoint m_lastPos;


protected:
	virtual void mousePressEvent(QMouseEvent* event) override;


	virtual void mouseReleaseEvent(QMouseEvent* event) override;


	virtual void mouseMoveEvent(QMouseEvent* event) override;

};
