#ifndef ErrCode_h__
#define ErrCode_h__

namespace ErrCode
{
	enum Code
	{
		succeeded = 0, //成功
		game_pro_exists, //游戏项目存在
		copy_engine_err,
		copy_template_err,
		replace_name_err,
	};
}


#endif // ErrCode_h__
