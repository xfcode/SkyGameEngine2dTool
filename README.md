# SkyGameEngine2dTool

#### 介绍

SkyGameEngine2d引擎的支持工具,对游戏引擎提供了 `创建游戏项目` `管理游戏项目` 等支持。

#### QQ交流群

群号：498358732

#### 游戏引擎项目

SkyGameEngine2d 是一款基于Directx11开发的2d游戏引擎。引擎简单、易用，提供中文文档以及教程支持，适合游戏开发入门的同学学习。

游戏引擎项目地址：[https://gitee.com/xfcode/SkyGameEngine2d](https://gitee.com/xfcode/SkyGameEngine2d)

#### 软件架构

windows平台，基于`Qt5.10`开发，Vs2015 开发工具集

#### 工具程序包下载

[https://dwz.cn/aWH5hM51](https://dwz.cn/aWH5hM51)

#### 引擎工具使用教程

[https://blog.csdn.net/qq_33775402/article/details/90478172](https://blog.csdn.net/qq_33775402/article/details/90478172)

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 软件界面展示

![输入图片说明](https://images.gitee.com/uploads/images/2019/0523/173516_31bc3a13_1401099.png "屏幕截图.png")
