#ifndef ProListModle_h__
#define ProListModle_h__

#include <QString>
#include <QList>
#include <QAbstractListModel>
#include "Tool.h"

struct ProjectData
{
	QString name;
	QString path;
	QString time;
};

class ProListModle
	: public QAbstractListModel, public Singleton<ProListModle>
{
	SINGLETON_CLASS(ProListModle)
	Q_OBJECT
private:
	QList<ProjectData> m_projectList;
public:

	Q_INVOKABLE virtual int rowCount(const QModelIndex& parent = QModelIndex()) const override
	{
		return m_projectList.size();
	}


	Q_INVOKABLE virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override
	{
		if (role == Qt::UserRole + 1)
		{
			return m_projectList.at(index.row()).name;
		}
		else if (role == Qt::UserRole + 2)
		{
			return m_projectList.at(index.row()).path;
		}
		else if (role == Qt::UserRole + 3)
		{
			return m_projectList.at(index.row()).time;
		}
		else
		{
			return QVariant();
		}
	}

	void appendProData(const ProjectData& data)
	{
		this->beginInsertRows(QModelIndex(), rowCount(), rowCount());
		m_projectList << data;
		this->endInsertRows();
	}

	void removeDate(int index)
	{
		this->beginRemoveRows(QModelIndex(), index, index);
		m_projectList.removeAt(index);
		this->endRemoveRows();
	}
};

#endif // ProListModle_h__
