#ifndef Config_h__
#define Config_h__

#include <QString>
#include "GloableData.h"
#include "Tinyxml2/tinyxml2.h"
#include "Tool.h"

class ProjectData;

class Config
	:public Singleton<Config>
{
	SINGLETON_CLASS(Config)
public:
	
	bool loadConfig(const QString& path);

	bool wirteConfig(const QString& path,const QString& key, const QString& value);

	bool appendProjectData(const QString& path, const ProjectData& data);

	bool removeProjectData(const QString& path,const QString& name);
private:
	
};


#endif // Config_h__
