#include "MainWidget.h"
#include "Tool.h"
#include "GloableData.h"
#include "./Tinyxml2/tinyxml2.h"
#include "TaskerNewProject.h"
#include "PagListDeleget.h"
#include "Config.h"
#include "ProListDeleget.h"
#include "ProListModle.h"
#include <QMouseEvent>
#include <QDesktopServices>
#include <QTime>
#include <QDateTime>

using namespace tinyxml2;

struct EngineInfo
{
	QString version;
};

QString parseEngineInfo(const XMLElement* root, EngineInfo* out)
{
	QString err_str;

	if (root)
	{
		auto version_e = root->FirstChildElement("version");
		if (version_e)
		{
			out->version = _STR(version_e->GetText());
		}
		else
		{
			err_str = _STR("解析版本信息失败");
		}
	}
	else
	{
		err_str = _STR("解析引擎信息文件失败");
	}
	return err_str;
}

////////////////////////////////////

MainWidget::MainWidget(QWidget* parent)
	: QMainWindow(parent)
	, m_isEnginePathOk(false)
	, m_taskerNewPro(nullptr)
	, m_isDrag(false)
	, m_isCreateProOpenProDir(true)
{
	ui.setupUi(this);

	this->setWindowFlags(this->windowFlags() | Qt::FramelessWindowHint);

	this->setAttribute(Qt::WA_TranslucentBackground, true);

	auto shadow = new QGraphicsDropShadowEffect(this);
	shadow->setOffset(0, 0);
	shadow->setColor(QColor("#474747"));
	shadow->setBlurRadius(50);
	ui.bgWidget->setGraphicsEffect(shadow);

	connect(ui.btnAddEnginePath, &QPushButton::clicked, this, &MainWidget::onBntAddEnginePath);
	connect(GloableData::getInstance(), &GloableData::changeEnginePath, this, &MainWidget::onChangeEnginePath);
	connect(GloableData::getInstance(), &GloableData::changeEngineVersion, this, &MainWidget::onChangeEngineVersion);
	connect(ui.btnCreateProject, &QPushButton::clicked, this, &MainWidget::onBntCreateNewGame);
	connect(ui.bntAddProjectDir, &QPushButton::clicked, this, &MainWidget::onBntAddProjectDir);
	connect(ui.bntClose, &QPushButton::clicked, this, &MainWidget::onBntClose);
	connect(ui.bntOpenGitCode, &QPushButton::clicked, this, []() {
		QUrl url(GloableData::getInstance()->getEngineGitCode());
		QDesktopServices::openUrl(url);
		});
	connect(ui.bntOpenDoc, &QPushButton::clicked, this, []() {
		QUrl url(GloableData::getInstance()->getEngineHelp());
		QDesktopServices::openUrl(url);
		});
	connect(ui.bntOpenGuide, &QPushButton::clicked, this, []() {
		QUrl url(GloableData::getInstance()->getEngineGuide());
		QDesktopServices::openUrl(url);
		});

	connect(ui.checkBoxOpenProDir, &QCheckBox::stateChanged, this, [this]() {
		m_isCreateProOpenProDir = ui.checkBoxOpenProDir->isChecked();
		});

	connect(ui.listWidget, &QListWidget::itemSelectionChanged, this, [this]() {
		int index = ui.listWidget->currentIndex().row();
		ui.stackedWidget->setCurrentIndex(index);
		});

	ui.stackedWidget->setCurrentIndex(0);

	ui.listWidget->setItemDelegate(new PagListDeleget);
	ui.listWidget->setItemSelected(ui.listWidget->item(0), true);

	ui.listWidgetProList->setItemDelegate(new ProjectListDelegate);
	ui.listWidgetProList->setModel(ProListModle::getInstance());
}


void MainWidget::onBntAddEnginePath()
{
	QString err_str;
	try
	{
		QString path = QFileDialog::getExistingDirectory(this, _STR("选择引擎目录"));

		if (path.isEmpty())
		{
			err_str = _STR("获取引擎目录失败，目录为空");
			throw std::exception();
		}
		else
		{
			GloableData::getInstance()->setEnginePath(path);
			bool ok = Config::getInstance()->wirteConfig(GloableData::getInstance()->getConfigPath(), _STR("last_engine_path"), path);
			if (!ok)
			{
				err_str = _STR("写入配置数据失败，无法记录本次打开的引擎路径");
				throw std::exception();
			}
		}
	}
	catch (...)
	{
		this->addErr(err_str);
		return;
	}
}

void MainWidget::onBntAddProjectDir()
{
	auto dir = QFileDialog::getExistingDirectory(this, _STR("选择目录"));
	if (dir.isEmpty())
	{
		this->addErr("项目目录不能为空");
		return;
	}

	ui.editProjectPath->setText(dir);
}

void MainWidget::onBntClose()
{
	this->close();
}

void MainWidget::addLog(const QString & log)
{
	static QString info_str = _STR("<span style=\"color:#5555ff; \">[info] %1: %2</span>");
	ui.LogWidget->append(info_str.arg(QTime::currentTime().toString()).arg(log));
}

void MainWidget::addLog(const char* log)
{
	this->addLog(_STR(log));
}

void MainWidget::addErr(const QString & err)
{
	static QString err_str = _STR("<span style=\"color:#ff0000; \">[err] %1: %2</span>");
	ui.LogWidget->append(err_str.arg(QTime::currentTime().toString()).arg(err));
}

void MainWidget::addErr(const char* err)
{
	this->addErr(_STR(err));
}

void MainWidget::onChangeEnginePath()
{
	QString err_str;
	auto path = GloableData::getInstance()->getEnginePath();
	try
	{
		auto info_file_name = GloableData::getInstance()->getEngineInfoFileName();

		auto info_path = path + "/" + info_file_name;
		if (!QFile::exists(info_path))
		{
			err_str = _STR("引擎信息文件不存在，可能是选择的引擎目录是错误的");
			throw std::exception();
		}

		tinyxml2::XMLDocument doc;
		auto err = doc.LoadFile(info_path.toStdString().c_str());
		if (err != tinyxml2::XML_SUCCESS)
		{
			err_str = _STR("解析引擎信息文件失败");
			throw std::exception();
		}

		EngineInfo info;
		XMLElement* root = doc.RootElement();
		err_str = parseEngineInfo(root, &info);
		if (!err_str.isEmpty())
		{
			throw std::exception();
		}

		ui.editEnginePath->setText(path);
		GloableData::getInstance()->setEngineVersion(info.version);
		this->addLog("获取引擎版本信息成功");
		m_isEnginePathOk = true;
		return;
	}
	catch (const std::exception&)
	{
		this->addErr(err_str);
		return;
	}
}

void MainWidget::onChangeEngineVersion()
{
	ui.labelVersion->setText(GloableData::getInstance()->getEngineVersion());
}

void MainWidget::onBntCreateNewGame()
{
	if (!m_isEnginePathOk)
	{
		this->addErr("引擎目录错误");
		return;
	}

	QString pro_name = ui.editProjectName->text();
	if (pro_name.isEmpty())
	{
		this->addErr("项目名字不能为空");
		return;
	}

	QString pro_dir = ui.editProjectPath->text();
	if (pro_dir.isEmpty())
	{
		this->addErr("新游戏的目录不能为空");
		return;
	}

	ui.btnCreateProject->setEnabled(false);
	ui.btnCreateProject->setText(_STR("正在创建项目..."));

	return createNewProject(pro_dir, pro_name);
}

void MainWidget::onCreateNewGameComplate(int code)
{
	ui.btnCreateProject->setEnabled(true);
	ui.btnCreateProject->setText(_STR("创建项目"));

	if (code != ErrCode::succeeded)
	{


		return;
	}

	this->addLog("创建项目成功");

	if (m_isCreateProOpenProDir)
	{
		QUrl url(m_currnetCreateProDir);
		bool ok = QDesktopServices::openUrl(url);
		if (ok)
		{
			this->addLog("打开项目目录成功");
		}
		else
		{
			this->addErr("打开项目目录遇到问题,打开失败");
		}
	}

	auto data = ProjectData
	{
		m_createProName,
		m_currnetCreateProDir.replace("//","/"),
		QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss")
	};

	ProListModle::getInstance()->appendProData(data);
	Config::getInstance()->appendProjectData(GloableData::getInstance()->getConfigPath(), data);
}

void MainWidget::createNewProject(const QString & dir, const QString & name)
{
	if (m_taskerNewPro == nullptr)
	{
		m_taskerNewPro = new TaskerNewProject();
		connect(m_taskerNewPro, &TaskerNewProject::complate, this, &MainWidget::onCreateNewGameComplate);
		typedef void (MainWidget:: * FunctionLog)(const QString&);
		FunctionLog pAddLog = &MainWidget::addLog;
		FunctionLog pAddErr = &MainWidget::addErr;
		connect(m_taskerNewPro, &TaskerNewProject::addErr, this, pAddErr);
		connect(m_taskerNewPro, &TaskerNewProject::addLog, this, pAddLog);
	}
	m_taskerNewPro->resetInfor(name, dir, GloableData::getInstance()->getEnginePath());
	m_currnetCreateProDir = dir + "/" + name;
	m_createProName = name;
	m_taskerNewPro->start();
}

void MainWidget::mousePressEvent(QMouseEvent * event)
{
	if (event->button() == Qt::LeftButton)
	{
		m_isDrag = true;
		m_lastPos = event->globalPos();
	}
}

void MainWidget::mouseReleaseEvent(QMouseEvent * event)
{
	if (event->button() == Qt::LeftButton)
	{
		m_isDrag = false;
	}
}

void MainWidget::mouseMoveEvent(QMouseEvent * event)
{
	if (m_isDrag)
	{
		auto dt = event->globalPos() - m_lastPos;
		this->move(this->pos() + dt);
		m_lastPos = event->globalPos();
	}
}

