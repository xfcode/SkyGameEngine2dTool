#ifndef TaskerNewProject_h__
#define TaskerNewProject_h__

#include <QThread>
#include <QFile>
#include <QDir>
#include <windows.h>
#include "Tool.h"
#include "ErrCode.h"
#include <QTextStream>

class TaskerNewProject
	:public QThread
{
	Q_OBJECT
public:

	TaskerNewProject()
	{
	}

	void resetInfor(const QString& pro_name, const QString& pro_dir, const QString& engine_path)
	{
		m_pro_dir = pro_dir;
		m_pro_name = pro_name;
		m_engine_path = engine_path;
	}

signals:

	void addLog(const QString& log);
	void addErr(const QString& err);
	void complate(int code);

protected:

	virtual void run() override
	{
		m_pro_path = m_pro_dir + "/" + m_pro_name;

		if (QFile::exists(m_pro_path))
		{
			emit this->complate(ErrCode::game_pro_exists);
			return;
		}

		emit this->addLog(_STR("创建游戏项目目录: %1").arg(m_pro_path));

		QDir dir;
		dir.cd(m_pro_dir);
		dir.mkdir(m_pro_name);

		emit this->addLog(_STR("正在copy引擎代码..."));

		//复制引擎
		auto err_code = copyEngine();
		if (err_code != ErrCode::succeeded)
		{
			emit this->complate(err_code);
			return;
		}

		emit this->addLog(_STR("copy引擎代码完成"));

		emit this->addLog(_STR("正在copy项目模板..."));

		//复制项目模板
		err_code = copyProjectTemplate();
		if (err_code != ErrCode::succeeded)
		{
			emit this->complate(err_code);
			return;
		}

		emit this->addLog(_STR("copy项目模板完成"));

		emit this->addLog(_STR("正在替换模板中项目的名字..."));

		//替换项目名字
		err_code = renameProjectName();
		if (err_code != ErrCode::succeeded)
		{
			emit this->complate(err_code);
			return;
		}

		emit this->addLog(_STR("替换模板中项目的名字完成"));

		emit this->complate(ErrCode::succeeded);
	}

private:
	//copy 引擎
	ErrCode::Code copyEngine()
	{
		try
		{
			//创建引擎目录
			QDir dir(m_pro_path);
			dir.mkdir("sky_game_engine2d");

			//头文件
			auto ok = FileHelper::copyDirectoryFiles(m_engine_path + "/include", m_pro_path + "/sky_game_engine2d/include", false);
			if (!ok)
			{
				throw std::exception();
			}
			emit this->addLog(_STR("copy [sky_game_engine2d/include] directory complete"));

			//src
			ok = FileHelper::copyDirectoryFiles(m_engine_path + "/src", m_pro_path + "/sky_game_engine2d/src", false);
			if (!ok)
			{
				throw std::exception();
			}
			emit this->addLog(_STR("copy [sky_game_engine2d/src] directory complete"));

			//build
			QStringList files;
			files << "SkyGameEngine2d.vcxproj" << "SkyGameEngine2d.vcxproj.filters";
			ok = FileHelper::copyDirectorySpecifiedFiles(m_engine_path + "/build", files, m_pro_path + "/sky_game_engine2d/project", false);
			if (!ok)
			{
				throw std::exception();
			}
			emit this->addLog(_STR("copy [sky_game_engine2d/project] directory complete"));
			return ErrCode::succeeded;
		}
		catch (...)
		{
			return ErrCode::copy_engine_err;
		}
	}
	//copy 模板
	ErrCode::Code copyProjectTemplate()
	{
		try
		{
			bool ok = FileHelper::copyDirectoryFiles(m_engine_path + "/empty_template", m_pro_path, false);
			if (!ok)
			{
				throw std::exception();
			}
			return ErrCode::succeeded;
		}
		catch (const std::exception&)
		{
			return ErrCode::copy_template_err;
		}
	}

	//更改游戏项目的名字
	ErrCode::Code renameProjectName()
	{
		auto err = this->renameProjectName(_STR("HelloWorld.sln"));
		if (err != ErrCode::succeeded)
		{
			return err;
		}

		err = this->renameProjectName(_STR("HelloWorld.vcxproj"));
		if (err != ErrCode::succeeded)
		{
			return err;
		}

		err = this->renameProjectName(_STR("HelloWorld.vcxproj.filters"));
		if (err != ErrCode::succeeded)
		{
			return err;
		}

		return ErrCode::succeeded;
	}
private:

	ErrCode::Code renameProjectName(const QString & file_name)
	{
		QString file_path = m_pro_path + "/build/" + file_name;
		QFile file(file_path);
		if (!file.open(QIODevice::ReadOnly| QIODevice::Text))
		{
			return ErrCode::replace_name_err;
		}

		QTextStream text(&file);
		QString str = text.readAll();
		str = str.replace("HelloWorld", m_pro_name);
		file.close();

		file.open(QIODevice::Truncate|QIODevice::WriteOnly);
		auto arr = str.toUtf8();
		file.write(arr,arr.length());
		file.close();

		file.rename(file.fileName().replace("HelloWorld", m_pro_name));
		return ErrCode::succeeded;
	}

	QString m_pro_name;
	QString m_pro_dir;
	QString m_engine_path;

	QString m_pro_path;
};



#endif // TaskerNewProject_h__
