#ifndef PagListDeleget_h__
#define PagListDeleget_h__

#include <QItemDelegate>
#include <QPainter>
#include <QPen>
#include <QColor>

class PagListDeleget
	:public QItemDelegate
{
	Q_OBJECT

private:

public:
	virtual void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override
	{
		painter->save();

		auto pen = painter->pen();
		auto bush = painter->brush();

		QFont font;
		font.setFamily("΢���ź�");
		font.setPointSize(11);
		painter->setFont(font);
		
		if (option.state & QStyle::State_Selected)
		{
			pen.setColor(QColor("#ececec"));
			painter->setPen(pen);
			
			auto rect = option.rect;
			painter->fillRect(rect,"#5d5d5d");
		}
		else
		{
			pen.setColor(QColor("#6e6e6e"));
			painter->setPen(pen);
		}
		
		auto rect = option.rect;
		painter->drawText(rect, Qt::AlignCenter, index.data(Qt::DisplayRole).toString());
		painter->restore();
	}


	virtual QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const override
	{
		return QSize(140, 35);
	}

};


#endif // PagListDeleget_h__
