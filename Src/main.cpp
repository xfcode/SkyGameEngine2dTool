﻿#include "MainWidget.h"
#include <QtWidgets/QApplication>
#include "Config.h"
#include <QMessageBox>
#include <cassert>

#define _MAIN_CPP
#include "ConfigTemplate.h"
#undef _MAIN_CPP

static void createConfigFile()
{
	QDir dir(QApplication::instance()->applicationDirPath());
	if (!dir.exists("conf"))
	{
		dir.mkdir("conf");
	}

	QFile file(GloableData::getInstance()->getConfigPath());
	if (!file.open(QIODevice::WriteOnly))
	{
		assert(false);
		return;
	}

	auto arr = cfg_template.toUtf8();
	file.write(arr,arr.length());
	file.close();

	return;
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWidget w;
	
	if (!QFile::exists(GloableData::getInstance()->getConfigPath()))
	{
		createConfigFile();
	}

	bool ok = Config::getInstance()->loadConfig(GloableData::getInstance()->getConfigPath());
	if (!ok)
	{
		QMessageBox::warning(&w, _STR("读取配置数据失败"), _STR("程序的部分功能可能无法使用"));
	}
	w.show();
	return a.exec();
}
