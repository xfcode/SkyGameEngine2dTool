#ifndef ProListDeleget_h__
#define ProListDeleget_h__

#include <QItemDelegate>
#include <QPen>
#include <QPainter>
#include <QIcon>

class ProjectListDelegate
	:public QItemDelegate
{

public:

	virtual void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override
	{
		painter->save();

		auto bush = painter->brush();
		auto pen_name = painter->pen();
		auto pen_path = painter->pen();
		auto font_name = painter->font();
		auto font_path = painter->font();

		font_name.setFamily("΢���ź�");
		font_name.setPixelSize(15);
		font_name.setWeight(QFont::DemiBold);
		font_path.setFamily("΢���ź�");
		font_path.setPixelSize(12);


		auto rect = option.rect;

		if (option.state & QStyle::State_MouseOver)
		{
			
			painter->fillRect(option.rect, "#9e9e9e");

			pen_name.setColor("#ffffff");
			pen_path.setColor("#eeeeee");
		}
		else if(option.state & QStyle::State_Selected)
		{
			painter->fillRect(option.rect, "#6e6e6e");

			pen_name.setColor("#ffffff");
			pen_path.setColor("#eeeeee");
		}
		else
		{
			pen_name.setColor("#6e6e6e");
			pen_path.setColor("#888888");
		}

		auto name_rect = rect.adjusted(10, 10, rect.width() - 200, 30);
		painter->setPen(pen_name);
		painter->setFont(font_name);
		painter->drawText(name_rect, Qt::AlignLeft, index.data(Qt::UserRole + 1).toString());

		auto path_rect = rect.adjusted(10, 35, 10, 10);
		painter->setPen(pen_path);
		painter->setFont(font_path);
		painter->drawText(path_rect, Qt::AlignLeft, index.data(Qt::UserRole + 2).toString());

		auto time_rect = rect.adjusted(rect.width() - 150, 10, 10, 35);
		painter->drawText(time_rect, Qt::AlignLeft, index.data(Qt::UserRole + 3).toString());



		painter->restore();
	}


	virtual QSize sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index) const override
	{
		return QSize(option.rect.width(), 60);
	}

};
#endif // ProListDeleget_h__
